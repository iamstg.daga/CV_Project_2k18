
    % Main script
    imagesdb = {'vangogh1.jpg','yellowhouse.jpg','starry_night_ref.jpg'};
    maximum_match = -1;
    maxmatched_image_idx = 1;
    current_frame = 'rotateSN.png';
    FindRPR(current_frame);      % whatever image is taken from camera
    rpr_frame = 'cropped.jpg';
    for i = 1 : size(imagesdb,2)
        ref_image = imagesdb{i};
        similarity = total_similarity(rpr_frame,ref_image);
        %disp(i);disp(similarity);
        if(similarity > maximum_match)
            maximum_match = similarity;
            maxmatched_image_idx = i;
        end
    end
    fprintf('%d : %s\n',maxmatched_image_idx,imagesdb{maxmatched_image_idx});
    Homography = estimateTransform(imagesdb{maxmatched_image_idx},current_frame);
    final_image = AR_Display(current_frame,maxmatched_image_idx,Homography);
    imshow(final_image);
