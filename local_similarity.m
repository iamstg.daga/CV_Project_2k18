function surf_similarity = local_similarity(image1,image2)
    original  = rgb2gray(imread(image1));
    distorted = rgb2gray(imread(image2));
    ptsOriginal  = detectSURFFeatures(original);
    ptsDistorted = detectSURFFeatures(distorted);
    [featuresOriginal,validPtsOriginal] = ...
        extractFeatures(original,ptsOriginal);
    [featuresDistorted,validPtsDistorted] = ...
        extractFeatures(distorted,ptsDistorted);
    index_pairs = matchFeatures(featuresOriginal,featuresDistorted);
    matchedPtsOriginal  = validPtsOriginal(index_pairs(:,1));
    matchedPtsDistorted = validPtsDistorted(index_pairs(:,2));
    %[tform,inlierPtsDistorted,inlierPtsOriginal] = ...
    %    estimateGeometricTransform(matchedPtsDistorted,matchedPtsOriginal,...
    %    'similarity');

    % calculating local similarity
    matches = size(index_pairs,1);
    eps = 0.01;
    sum = 0;
    for i = 1 : matches
        dist = norm(featuresOriginal(index_pairs(i,1),:) - featuresDistorted(index_pairs(i,2),:));
        sf = 1.0 / (1.0 + dist);
        sum = sum + sf;
    end
    surf_similarity = sum / (eps + matches) ;
    %disp(surf_similarity);
end