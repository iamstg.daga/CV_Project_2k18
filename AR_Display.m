function finalim = AR_Display(image_file,idx,H)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AR DISPLAY CODE  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    corresponding_text = {'vangogh village painting','van gogh picture of yellow house','Starry Night by Van Gogh'};
    im = imread(image_file);
    %H = [1.0006030 -6.0462342e-05 0; 6.0462342e-05 1.0006030 0; 64.952072 35.019203 1];
    %H = [27.280621 -43.000736 0; 43.000736 27.280621 0; -4580.1572 3092.6118 1];
    %H = [2.7062 0.4641 0; -0.4641 2.7062 0; -66.4951 -81.7697 1];
    text = corresponding_text(idx);
    %H = [1 1 0; 1.2 0.3 0; 1.5 1.6 1];
    textim = insertText(zeros(500,700),[220 200],text,'BoxOpacity',0,'FontSize',25,'TextColor','w');
    %textim = text2im(text);
    %figure;
    %imshow(textim);
    [x ,y, ~]=size(textim);
 
    %for i=1:x+100
    %    for j =1:y+100
    %        X = [i j 1];
    %        Y = transpose(X);
            %P = transpose(X*H);
    %        P = inv(H)*Y;
    %        disp(P(3,1));
    %        newim(uint8(P(1,1)/P(3,1)),uint8(P(2,1)/P(3,1))) = textim(i,j);
    %    end
    %end
    %imshow(newim);
    %textim = newim;
    %textim(:,:,1) = temp;
    %textim(:,:,2) = temp;
    %textim(:,:,3) = temp;
    %H = inv(H);
    %H(2,3) = 0;
    %H(1,3) = 0;

    disp(H);
    %A = affine2d(H);
    A = H;
    %t = maketform('projective',H);

    %transformed = imtransform(textim,t);
    transformed = imwarp(textim,A);
    [x, y, ~] = size(textim);

    transformed = imresize(transformed,[x y]);

    %figure;
    %imshow(textim);
    %figure;
    %imshow(transformed);
    for i=1:size(im,1)
        for j =1:size(im,2)
            if i<size(transformed,1) && j<size(transformed,2)&& transformed(i,j)~=0
                finalim(i,j,1) = uint8(255*transformed(i,j,1));
                finalim(i,j,2) = uint8(255*transformed(i,j,2));
                finalim(i,j,3) = uint8(255*transformed(i,j,3));
            else
                finalim(i,j,1) = im(i,j,1);
                finalim(i,j,2) = im(i,j,2);
                finalim(i,j,3) = im(i,j,3);
            end
        end
    end
    %figure,imshow(finalim);
    %figure,imshowpair(im,finalim);
    %figure,imshowpair(im,textim);
end