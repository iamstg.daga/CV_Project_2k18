function final = total_similarity(image1,image2)
    alpha = 0.28;
    beta = 1 - alpha ;
    localS = local_similarity(image1,image2);
    globalS = global_similarity(image1,image2);
    final = (alpha * localS) + (beta * globalS);
    %disp(final);
end