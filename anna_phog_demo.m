function p = anna_phog_demo(I)
    bin = 8;
    angle = 360;
    L=3;
    roi = [1;101;1;135];
    p = anna_phog(I,bin,angle,L,roi);
end
