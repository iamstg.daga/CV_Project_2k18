function phog_similarity = global_similarity(image1,image2)
    % PHOG Similarity
    phog_feature1 = anna_phog_demo(image1);
    phog_feature2 = anna_phog_demo(image2);
    lambda = 0.5;
    phog_similarity = 1 / (1 + (lambda * norm(phog_feature1 - phog_feature2)));
    %disp(phog_similarity);
end