function tform = estimateTransform(orignal , distorted)
    % original : dbImage
    % distorted : current camera frame
    original  = rgb2gray(imread(orignal));
    distorted =rgb2gray(imread(distorted));
    ptsOriginal  = detectSURFFeatures(original);
    ptsDistorted = detectSURFFeatures(distorted);
    [featuresOriginal,validPtsOriginal] = ...
        extractFeatures(original,ptsOriginal);
    [featuresDistorted,validPtsDistorted] = ...
        extractFeatures(distorted,ptsDistorted);
    index_pairs = matchFeatures(featuresOriginal,featuresDistorted);
    matchedPtsOriginal  = validPtsOriginal(index_pairs(:,1));
    matchedPtsDistorted = validPtsDistorted(index_pairs(:,2));
    [tform,inlierPtsDistorted,inlierPtsOriginal] = ...
        estimateGeometricTransform(matchedPtsDistorted,matchedPtsOriginal,...
        'similarity');
end
